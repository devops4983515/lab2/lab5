FROM alpine:3.14 AS system
RUN apk add --no-cache \
sqlite \
sqlite-dev \
&& apk add --no-cache --virtual .build-deps \
gcc \
musl-dev \
&& apk add --no-cache bash

FROM python:3.9-alpine AS build
RUN apk add --no-cache \
build-base \
&& pip install --upgrade pip
WORKDIR /project
COPY ./app/requirements.txt /project
RUN pip install -r requirements.txt

FROM python:3.9-alpine AS app
ENV PYTHONUNBUFFERED 1
WORKDIR /project
COPY --from=build /usr/local/lib/python3.9/site-packages /usr/local/lib/python3.9/site-packages
COPY ./app /project
